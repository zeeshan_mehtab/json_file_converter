<?php

namespace App\Command;

use App\Services\Converters\JsonlFileConverterInterface;
use App\Services\Downloaders\OrdersFileDownloaderInterface;
use Symfony\Component\Console\Command\Command;
use Symfony\Component\Console\Input\InputArgument;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Output\OutputInterface;
use Symfony\Component\Console\Style\SymfonyStyle;

/**
 * Service to download jsonl file for s3 and parse them to extract a csv report.
 */
class JsonlToCsvOrderSummaryGeneratorCommand extends Command
{
    protected static $defaultName = 'app:jsonl-to-csv:order-summary-generator';
    protected static $defaultDescription = 'Console command to download orders.jsonl from s3 and generate an order summary in csv format';
    /**
     * @var OrdersFileDownloaderInterface
     */
    private $ordersReader;
    /**
     * @var JsonlFileConverterInterface
     */
    private $ordersWriter;

    public function __construct(OrdersFileDownloaderInterface $ordersReader, JsonlFileConverterInterface $ordersWriter)
    {
        parent::__construct(null);
        $this->ordersReader = $ordersReader;
        $this->ordersWriter = $ordersWriter;
    }

    protected function configure(): void
    {
        $this
            ->addArgument('filePath', InputArgument::OPTIONAL, 'JSON File Path');
    }

    protected function execute(InputInterface $input, OutputInterface $output): int
    {
        $io = new SymfonyStyle($input, $output);
        $filePath = $input->getArgument('filePath');

        if (null !== $filePath) {

            $io->text('<info>JSON File Path</info>: ' . $filePath);
        } else {

            $filePath = $io->ask('Enter JSON File Path:');
        }

        $fileReaderResponse = $this->ordersReader->downloadFile($filePath);

        if ($fileReaderResponse["code"] == "success") {

            $io->info("JSON file downloaded successfully");

            $filePath = $fileReaderResponse["message"];

            $this->ordersWriter->convertFile($filePath);

            return Command::SUCCESS;

        } else {

            $io->error($fileReaderResponse["message"]);

            return Command::FAILURE;
        }


    }
}
