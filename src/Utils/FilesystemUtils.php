<?php

namespace App\Utils;

use Symfony\Component\Filesystem\Filesystem;

class FilesystemUtils
{

    /**
     * @var Filesystem
     */
    private $filesystem;
    /**
     * @var string
     */
    private $projectBaseDir;

    public function __construct(Filesystem $filesystem, string $projectBaseDir)
    {

        $this->filesystem = $filesystem;
        $this->projectBaseDir = $projectBaseDir;
    }

    public function getTempFilePath($fileName): string
    {
        $destinationDir = $this->projectBaseDir . DIRECTORY_SEPARATOR . "public" . DIRECTORY_SEPARATOR . "temp_files";

        if (!$this->filesystem->exists($destinationDir)) {

            $this->filesystem->mkdir($destinationDir, 0775);
        }

        return $destinationDir . DIRECTORY_SEPARATOR . $fileName;
    }

}