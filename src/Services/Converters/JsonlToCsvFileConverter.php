<?php

namespace App\Services\Converters;

use App\Services\DataHandlers\JsonltoCsvOrderRowHandler;
use App\Utils\FilesystemUtils;
use Symfony\Component\Filesystem\Exception\IOExceptionInterface;
use Symfony\Component\Filesystem\Filesystem;

class JsonlToCsvFileConverter implements JsonlFileConverterInterface
{

    /**
     * @var Filesystem
     */
    private $fileSystem;
    /**
     * @var FilesystemUtils
     */
    private $filesystemUtils;

    public function __construct(Filesystem $fileSystem, FilesystemUtils $filesystemUtils)
    {

        $this->fileSystem = $fileSystem;
        $this->filesystemUtils = $filesystemUtils;
    }

    /**
     * @inheritDoc
     */
    public function convertFile(string $sourceFilePath): array
    {

        try {

            $destinationFilePath = $this->filesystemUtils->getTempFilePath("out.csv");

            $this->fileSystem->remove($destinationFilePath);
            $this->fileSystem->touch($destinationFilePath);

            $handle = fopen($sourceFilePath, "r") or die("Couldn't get handle");

            if ($handle) {

                while (!feof($handle)) {

                    $line = fgets($handle);
                    $order = json_decode($line, true);

                    if (!$order) {
                        continue;
                    }

                    $row = new JsonltoCsvOrderRowHandler($order);

                    if ($row->validate()) {

                        $row = $row->getOutput();

                        if (!empty($row)) {

                            $this->fileSystem->appendToFile($destinationFilePath, $row);
                        }
                    } else {
                        echo "Invalid Row";
                    }

                }
                fclose($handle);
            }

        } catch (IOExceptionInterface $ioException) {

            return [
                "code" => "error",
                "message" => "Error creating file at" . $ioException->getPath()
            ];
        }

        return [
            "code" => "success",
            "message" => "Order summary generated successfully."
        ];
    }

    private function calculateDiscount($amount, $discount, $type): float
    {

        if ($type == "PERCENTAGE") {

            return round(($amount / 100 * $discount), 2);
        }

        return $discount;

    }

}