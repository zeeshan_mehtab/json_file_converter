<?php

namespace App\Services\Converters;

interface JsonlFileConverterInterface
{
    /**
     * @param string $sourceFilePath the path of the source file
     * @return array
     */
    public function convertFile(string $sourceFilePath): array;

}