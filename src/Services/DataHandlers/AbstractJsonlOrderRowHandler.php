<?php

namespace App\Services\DataHandlers;

abstract class AbstractJsonlOrderRowHandler
{

    protected $orderId;
    protected $totalOrderValue;
    protected $totalUnits;
    protected $orderDateTime;
    protected $customerState;
    protected $averageUnitPrice;
    protected $distinctUnitsCount;

    public function __construct(array $order)
    {
        $this->initialize($order);
    }

    public function initialize(array $order): void
    {
        $items = $order['items'] ?? [];
        $unitCodes = [];

        foreach ($items as $item) {

            $this->totalOrderValue += ($item['quantity'] * $item['unit_price']);
            $this->totalUnits += $item['quantity'];
            array_push($unitCodes, $item['product']['product_id']);

        }

        $discounts = $order['discounts'] ?? [];
        // Sort by priority asc
        usort($discounts, function ($a, $b) {

            return $a['priority'] > $b['priority'];
        });

        foreach ($discounts as $discount) {

            $discountValue = $this->calculateDiscount($this->totalOrderValue, $discount['value'], $discount['type']);
            $this->totalOrderValue -= $discountValue;
        }

        $this->orderId = $order["order_id"];
        $orderDateTime = $order['order_date'] ?? '';
        $dateTime = strtotime($orderDateTime);
        $this->orderDateTime = date('Y-m-d\TH:i:sO', $dateTime);
        $this->customerState = $order['customer']['shipping_address']['state'] ?? '';

        $this->averageUnitPrice = round($this->totalOrderValue / $this->totalUnits, 2);
        $this->distinctUnitsCount = count(array_unique($unitCodes));
    }

    private function calculateDiscount($amount, $discount, $type): float
    {

        if ($type == "PERCENTAGE") {

            return round(($amount / 100 * $discount), 2);
        }

        return $discount;

    }

    abstract public function validate(): bool;

    abstract public function getOutput(): string;

}