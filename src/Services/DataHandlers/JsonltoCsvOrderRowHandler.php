<?php

namespace App\Services\DataHandlers;

class JsonltoCsvOrderRowHandler extends AbstractJsonlOrderRowHandler
{

    public function validate(): bool
    {
        // TODO: Implement validate() method.
        return true;
    }

    public function getOutput(): string
    {
        $output = "";

        if ($this->totalOrderValue > 0 && $this->orderId) {

            $output = "{$this->orderId},{$this->orderDateTime},{$this->totalOrderValue},{$this->averageUnitPrice},{$this->distinctUnitsCount},{$this->totalUnits},{$this->customerState}\r\n";
        }

        return $output;

    }
}