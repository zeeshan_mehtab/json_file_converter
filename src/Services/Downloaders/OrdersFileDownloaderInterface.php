<?php

namespace App\Services\Downloaders;

interface OrdersFileDownloaderInterface
{
    /**
     * @param string $filePath Absolute path to read the orders
     * @return array Associative array containing all orders
     */
    public function downloadFile(string $filePath): array;
}