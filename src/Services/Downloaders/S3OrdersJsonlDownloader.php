<?php

namespace App\Services\Downloaders;

use App\Utils\FilesystemUtils;
use Symfony\Component\Filesystem\Exception\IOExceptionInterface;
use Symfony\Component\Filesystem\Filesystem;
use Symfony\Contracts\HttpClient\HttpClientInterface;

class S3OrdersJsonlDownloader implements OrdersFileDownloaderInterface
{

    /**
     * @var HttpClientInterface
     */
    private $httpClient;
    /**
     * @var Filesystem
     */
    private $filesystem;
    /**
     * @var FilesystemUtils
     */
    private $filesystemUtils;

    public function __construct(HttpClientInterface $httpClient, Filesystem $filesystem, FilesystemUtils $filesystemUtils)
    {

        $this->httpClient = $httpClient;
        $this->filesystem = $filesystem;
        $this->filesystemUtils = $filesystemUtils;
    }

    /**
     * @inheritDoc
     */
    public function downloadFile(string $filePath): array
    {
        //https://s3-ap-southeast-2.amazonaws.com/catch-code-challenge/challenge-1/orders.jsonl

        $response = $this->httpClient->request(
            "GET",
            $filePath,
            [
                "headers" => [
                    "Content-Type" => "application/json"
                ]
            ]
        );

        $responseCode = $response->getStatusCode();

        if ($responseCode != 200) {
            return [
                "code" => "error",
                "message" => "Unable to read jsonl file from specified URL"
            ];
        }

        $content = $response->getContent();

        // Build unique file name to save in temp directory
        $currentMilliSeconds = round(microtime(true) * 1000);
        $fileName = "orders_{$currentMilliSeconds}.jsonl";

        $filePath = $this->filesystemUtils->getTempFilePath($fileName);

        try {

            // Write jsonl content to the temp file
            $this->filesystem->touch($filePath);
            $this->filesystem->chmod($filePath, 0777);
            $this->filesystem->dumpFile($filePath, $content);

        } catch (IOExceptionInterface $ioException) {

            return [
                "code" => "error",
                "message" => $ioException->getPath()
            ];
        }

        return [
            "code" => "success",
            "message" => $filePath
        ];

    }
}