JSONL to CSV Convertor
========================
The Application is developed using Symfony framework [Symfony Best Practices][1].

Developer Details
------------
* Name: Zeeshan Mehtab
* Email: zeeshan.mehtab1@gmail.com
* Phone: 0421 330 485

Requirements
------------

  * PHP 7.3 or higher;
  * PDO-SQLite PHP extension enabled;
  * and the [usual Symfony application requirements][2].

Installation
------------

Using Git clone the project from repo https://bitbucket.org/zeeshan_mehtab/json_file_converter/src/master/

```bash
$ git clone git@bitbucket.org:zeeshan_mehtab/json_file_converter.git json_file_converter
```
Run composer install to pull dependencies

```bash
$ cd json_file_converter/
$ composer install
```

Execute following command to downlad jsonl file from s3 and convert it into a csv file. The csv file out.csv is stored in {project_root}/public/temp_files

```bash
$ cd json_file_converter/
$ php bin/console app:jsonl-to-csv:order-summary-generator https://s3-ap-southeast-2.amazonaws.com/catch-code-challenge/challenge-1/orders.jsonl
```

The command required one parameter which is the URL of the s3 file (https://s3-ap-southeast-2.amazonaws.com/catch-code-challenge/challenge-1/orders.jsonl)

Tests
-----

```bash
$ cd my_project/
$ ./bin/phpunit
```

[1]: https://symfony.com/doc/current/best_practices.html
[2]: https://symfony.com/doc/current/reference/requirements.html
[3]: https://symfony.com/doc/current/cookbook/configuration/web_server_configuration.html
[4]: https://symfony.com/download
